package ru.klaw.restlog.service;

import org.junit.jupiter.api.Test;
import ru.klaw.restlog.model.Log;
import ru.klaw.restlog.model.LogFilter;
import ru.klaw.restlog.model.Severity;
import ru.klaw.restlog.utility.filter.Filter;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LogFiltersTest {

    @Test
    void createFiltersFrom() {
        final LogFilter filter = new LogFilter("Ts", Severity.WARNING,
                "SsN", "MN", "SN",
                "TI", "UI", "TI", "DCI",
                "RT", "MI", "Text", "RegText");
        final List<Filter<Log>> filtersFrom = LogFilters.createFiltersFrom(filter);

        assertEquals(13,filtersFrom.size());
    }
}