package ru.klaw.restlog.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;

class FileServiceTest {

    private EnvironmentService environment;
    private FileService service;
    private String resourcePath = Objects.requireNonNull(this.getClass().getClassLoader().getResource("service")).getPath();

    @BeforeEach
    void setUp() {
        environment = Mockito.mock(EnvironmentService.class);
        final String path = new File(resourcePath).toPath().toString();
        Mockito.when(environment.getSystemEnvironment(eq("DOMAIN_HOME"))).thenReturn(path);
        Mockito.when(environment.getSystemEnvironment(eq("SERVER_NAME"))).thenReturn("ServerName");
        service = new FileService(environment);
    }

    @Test
    void getLogFileArray() {

        final FileService.FileMetaData[] serverNames = this.service.getLogFileArray("ServerName");
        assertEquals(2,serverNames.length);
        assertNotNull(serverNames[0].file);
    }
}