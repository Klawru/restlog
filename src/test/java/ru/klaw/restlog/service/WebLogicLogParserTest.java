package ru.klaw.restlog.service;

import org.junit.jupiter.api.Test;
import ru.klaw.restlog.model.Log;
import ru.klaw.restlog.model.Severity;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WebLogicLogParserTest {

    @Test
    void parseLine() {
        final Log log = WebLogicLogParser.parseLine("####<25.05.2020 21:24:16,073 MSK> <Info> <Subsystem> <MachineName> <ServerName> <ThreadID> <<User ID>> <> <3fa7a048-e9cb-420a-a7de-9df95f3a8fbe-00000004> <1590431056073> <[severity-value: 64] [rid: 0] > <BEA-111111> <Text> ");
        assertEquals(Severity.INFO, log.getSeverity());
        assertEquals("Subsystem", log.getSubsystem());
        assertEquals("MachineName", log.getMachine());
        assertEquals("ServerName", log.getServer());
        assertEquals("ThreadID", log.getThreadId());
        assertEquals("<User ID>", log.getUserId());
        assertEquals("", log.getTransactionId());
        assertEquals("3fa7a048-e9cb-420a-a7de-9df95f3a8fbe-00000004", log.getDiagnosticContextId());
        assertEquals("1590431056073", log.getRawTime());
        assertEquals("BEA-111111", log.getMessageId());
        assertEquals("Text", log.getText());
    }

    @Test
    void parseLine2() {
        final Log log = WebLogicLogParser.parseLine("Unexpected line");
        assertEquals(log.getText(), "Unexpected line");
    }

    @Test
    void parseLine3() {
        final Log log = WebLogicLogParser.parseLine("####<25.05.2020 21:24:16,073 MSK> <Info> <Subsystem> <MachineName> <ServerName> <ThreadID> <<User ID>LostData> <> <3fa7a048-e9cb-420a-a7de-9df95f3a8fbe-00000004> <1590431056073> <[severity-value: 64] [rid: 0] > <BEA-111111> <Text>");
        assertEquals("<User ID>LostData", log.getUserId());
    }
}