package ru.klaw.restlog.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.klaw.restlog.model.Log;
import ru.klaw.restlog.model.Severity;
import ru.klaw.restlog.service.LogFileParser;
import ru.klaw.restlog.utility.mapper.LogMapper;
import ru.klaw.restlog.utility.mapper.LogMapperImpl;
import ru.klaw.soap.model.GetLogRequest;
import ru.klaw.soap.model.GetLogResponse;
import ru.klaw.soap.model.LogFilterRequestType;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;


class LogEndpointTest {

    private LogFileParser service = Mockito.mock(LogFileParser.class);

    private Log testLog = new Log("Ts", Severity.WARNING,
            "SsN", "MN", "SN",
            "TI", "UI", "TI", "DCI",
            "RT", "MI", "Text");

    private LogEndpoint logEndpoint;

    private final LogMapper logMapper = new LogMapperImpl();

    @BeforeEach
    void before() {
        logEndpoint = new LogEndpoint(service,logMapper);
    }

    @Test
    void getLogRequest() {
        final GetLogRequest getLogRequest = new GetLogRequest();
        final LogFilterRequestType logFilterRequestType = new LogFilterRequestType();
        logFilterRequestType.setResultSize(1000);
        logFilterRequestType.setLineRead(5000);
        logFilterRequestType.setSkipLine(0);
        logFilterRequestType.setServerName("0");
        getLogRequest.setFilter(logFilterRequestType);

        final Log[] result = {testLog};

        Mockito.when(service.findLog(isNull(),eq("0"),anyInt(),anyInt(),anyInt(),isNull())).thenReturn(result);

        final GetLogResponse logRequest = logEndpoint.getLogRequest(getLogRequest);
        assertNotNull(logRequest);
        assertNotNull(logRequest.getLog());
        assertEquals(logRequest.getLog().size(),1);
        assertEquals(logRequest.getLog().get(0).getText(),testLog.getText());
    }
}