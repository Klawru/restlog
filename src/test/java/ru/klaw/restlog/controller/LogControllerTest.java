package ru.klaw.restlog.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.klaw.restlog.model.Log;
import ru.klaw.restlog.model.LogFilter;
import ru.klaw.restlog.model.Severity;
import ru.klaw.restlog.service.LogFileParser;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = LogController.class)
class LogControllerTest {

    private final String URI = "/getLog";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LogFileParser service;

    private Log testLog = new Log("Ts", Severity.WARNING,
            "SsN", "MN", "SN",
            "TI", "UI", "TI", "DCI",
            "RT", "MI", "Text");

    @Test
    void getLogWithoutParam() throws Exception {
        final Log[] result = new Log[]{testLog, testLog};
        Mockito.when(service.findLog(isNull(), isNull(), anyInt(), anyInt(), anyInt(), isNull())).thenReturn(result);
        mockMvc.perform(post(URI))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[1].text", is(result[0].getText())));
    }

    @Test
    void getLog() throws Exception {
        final Log[] result = new Log[]{testLog};
        final LogFilter filter = new LogFilter();
        Mockito.when(service.findLog(eq(filter), isNull(), anyInt(), anyInt(), anyInt(), isNull())).thenReturn(result);
        mockMvc.perform(post(URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(filter)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(1)))
                .andExpect(jsonPath("$[0].text", is(result[0].getText())));
    }

    public static String asJsonString(final Object obj) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(obj);
    }
}