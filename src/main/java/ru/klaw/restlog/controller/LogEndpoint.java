package ru.klaw.restlog.controller;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.klaw.restlog.model.Log;
import ru.klaw.restlog.utility.mapper.LogMapper;
import ru.klaw.restlog.service.LogFileParser;
import ru.klaw.soap.model.GetLogRequest;
import ru.klaw.soap.model.GetLogResponse;
import ru.klaw.soap.model.LogFilterRequestType;

import java.util.Arrays;

@Endpoint
public class LogEndpoint {
    private static final String NAMESPACE_URI = "ru.klaw";

    private final LogFileParser logFileParser;
    private final LogMapper logMapper;

    public LogEndpoint(LogFileParser logFileParser, LogMapper logMapper) {
        this.logFileParser = logFileParser;
        this.logMapper = logMapper;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getLogRequest")
    @ResponsePayload
    public GetLogResponse getLogRequest(@RequestPayload GetLogRequest logRequest) {
        final LogFilterRequestType filter = logRequest.getFilter();
        final Log[] logs = logFileParser.findLog(logMapper.toLogFilter(filter.getFilter()),
                filter.getServerName(),
                filter.getResultSize(),
                filter.getSkipLine(),
                filter.getLineRead(),
                logMapper.toLocalDateTime(filter.getAfter()));

        final GetLogResponse response = new GetLogResponse();
        response.getLog().addAll(logMapper.toListLogType(Arrays.asList(logs)));
        return response;
    }

}
