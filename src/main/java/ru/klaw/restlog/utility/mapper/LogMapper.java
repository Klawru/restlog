package ru.klaw.restlog.utility.mapper;

import org.mapstruct.Mapper;
import ru.klaw.restlog.model.Log;
import ru.klaw.restlog.model.LogFilter;
import ru.klaw.soap.model.LogFilterType;
import ru.klaw.soap.model.LogType;

import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDateTime;
import java.util.List;

@Mapper(componentModel = "spring")
public interface LogMapper {
    LogType toLogType(Log log);

    List<LogType> toListLogType(List<Log> log);

    LogFilter toLogFilter(LogFilterType filter);

    default LocalDateTime toLocalDateTime(XMLGregorianCalendar date) {
        if (date != null)
            return date.toGregorianCalendar().toZonedDateTime().toLocalDateTime();
        return null;
    }
}
