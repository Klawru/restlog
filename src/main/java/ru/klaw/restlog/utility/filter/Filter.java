package ru.klaw.restlog.utility.filter;

public interface Filter<T> {
    /**
     * Checks to see if the object should be accepted by this filter.
     *
     * @param log  the object to check.
     * @return true if this object matches the test.
     */
    boolean accept(T log);
}
