package ru.klaw.restlog.utility.filter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public abstract class TimeFilter<T> implements Filter<T> {
    private final Instant filter;

    public TimeFilter(LocalDateTime filter) {
        this.filter = filter.toInstant(ZoneOffset.UTC);
    }

    protected Instant getInstant(String rawTime){
        return Instant.ofEpochMilli(Long.parseLong(rawTime));
    }

    protected boolean isBefore(Instant time){
        return filter.isBefore(time);
    }

    protected boolean isAfter(Instant time){
        return filter.isAfter(time);
    }
}
