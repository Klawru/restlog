package ru.klaw.restlog.utility.filter;

import java.util.ArrayList;
import java.util.List;

public class ArrayFilter<T> implements Filter<T> {

    private final List<Filter<T>> filters;

    public ArrayFilter(List<Filter<T>> filters) {
        this.filters = new ArrayList<>(filters);
    }

    @Override
    public boolean accept(T log) {
        for (Filter<T> filter : filters) {
            if (!filter.accept(log))
                return false;
        }
        return true;
    }
}
