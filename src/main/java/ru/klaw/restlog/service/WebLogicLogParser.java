package ru.klaw.restlog.service;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import ru.klaw.restlog.model.Log;
import ru.klaw.restlog.model.Severity;

import java.util.Arrays;

import static ru.klaw.restlog.model.Severity.*;

@Slf4j
@UtilityClass
public class WebLogicLogParser {

    static public Log parseLine(String line) {
        try {
            final String[] s = getSegments(line);
            //10 сегмент игнорируеться;
            return new Log(s[0], parseSeverity(s[1]), s[2], s[3], s[4], s[5], s[6], s[7], s[8], s[9], s[11], s[12]);
        } catch (Exception e) {
            log.error("Error on line='{}'", line);
            throw e;
        }
    }

    static private String[] getSegments(String line) {
        String[] result = line.split("> <");
        //Если подана строка в неверном формате
        if (result.length != 13)
            return putLineToLastSegment(line);
        //Убираем стандартный префикс
        if (result[0].startsWith("####<"))
            result[0] = result[0].substring(5);
        //Обрезаем осташийся суфикс
        if (result[12].endsWith("> "))
            result[12] = result[12].substring(0,result[12].length()-2);
        return result;
    }

    private static String[] putLineToLastSegment(String line) {
        final String[] result = new String[13];
        Arrays.fill(result, "");
        result[12] = line;
        return result;
    }

    public static Severity parseSeverity(String severity) {
        switch (severity) {
            case "Trace":
                return TRACE;
            case "Debug":
                return DEBUG;
            case "Info":
                return INFO;
            case "Notice":
                return NOTICE;
            case "Warning":
                return WARNING;
            case "Error":
                return ERROR;
            case "Critical":
                return CRITICAL;
            case "Alert":
                return ALERT;
            case "Emergency":
                return EMERGENCY;
        }
        return null;
    }
}
