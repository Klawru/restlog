package ru.klaw.restlog.service;

import com.google.common.base.Charsets;
import org.apache.commons.io.input.ReversedLinesFileReader;
import org.springframework.stereotype.Service;
import ru.klaw.restlog.model.Log;
import ru.klaw.restlog.model.LogFilter;
import ru.klaw.restlog.service.FileService.FileMetaData;
import ru.klaw.restlog.utility.filter.ArrayFilter;
import ru.klaw.restlog.utility.filter.Filter;
import ru.klaw.restlog.utility.filter.TimeFilter;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;

import static ru.klaw.restlog.service.WebLogicLogParser.parseLine;

@Service
public class LogFileParser {

    private final FileService fileService;

    public LogFileParser(FileService fileService) {
        this.fileService = fileService;
    }

    public Log[] findLog(LogFilter logFilter, String serverName,
                         final int resultSize, int skipLine, int lineRead,
                         final LocalDateTime after) {
        final FileMetaData[] logFileArray = fileService.getLogFileArray(serverName);
        final Filter<Log> filters = new ArrayFilter<>(LogFilters.createFiltersFrom(logFilter));
        final Filter<FileMetaData> afterTimestampFilter = getAfterTimestampFilter(after);
        final Log[] result = new Log[resultSize];

        int resultIndex = 0;
        //Цикл по файлам
        for (int i = 0; i < logFileArray.length && resultIndex < resultSize && lineRead>0; i++) {
            if (afterTimestampFilter != null && !afterTimestampFilter.accept(logFileArray[i]))
                return result;
            try (ReversedLinesFileReader logReader = getLogReader(logFileArray[i])) {
                //Чтение файла
                String line = readLine(logReader);
                while (line != null && resultIndex < resultSize) {
                    lineRead--;
                    final Log log = parseLine(line);
                    if (filters.accept(log)) {
                        if (skipLine > 0)
                            skipLine--;
                        else
                            result[resultIndex++] = log;

                    }
                    line = readLine(logReader);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return reverse(truncateResult(result, resultIndex));
    }

    private<T> T[] reverse(T[] array) {
        for(int i = 0; i < array.length / 2; i++)
        {
            T temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
        }
        return array;
    }

    private Log[] truncateResult(Log[] result, int resultIndex) {
        if (result.length == resultIndex)
            return result;
        else
            return Arrays.copyOf(result, resultIndex);
    }

    private ReversedLinesFileReader getLogReader(FileMetaData fileMetaData) throws IOException {
        return new ReversedLinesFileReader(fileMetaData.file, 4 * 1024 * 128, Charsets.UTF_8);
    }


    private static String readLine(ReversedLinesFileReader logReader) throws IOException {
        String line = logReader.readLine();
        if (line == null)
            return null;
        if (line.startsWith("####"))
            return line;
        else {
            StringBuilder result = new StringBuilder();
            while (line != null && !line.startsWith("####")) {
                result.insert(0, line);
                line = logReader.readLine();
            }
            result.insert(0, line);
            return result.toString();
        }
    }

    private static Filter<FileMetaData> getAfterTimestampFilter(LocalDateTime filter) {
        if (Objects.isNull(filter))
            return null;
        return new TimeFilter<FileMetaData>(filter) {
            @Override
            public boolean accept(FileMetaData fileMetaData) {
                return isAfter(fileMetaData.start);
            }
        };
    }

}
