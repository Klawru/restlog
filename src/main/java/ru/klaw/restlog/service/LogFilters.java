package ru.klaw.restlog.service;

import lombok.experimental.UtilityClass;
import ru.klaw.restlog.model.Log;
import ru.klaw.restlog.model.LogFilter;
import ru.klaw.restlog.model.Severity;
import ru.klaw.restlog.utility.filter.Filter;
import ru.klaw.restlog.utility.filter.RegexFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@UtilityClass
public class LogFilters {

    public List<Filter<Log>> createFiltersFrom(LogFilter logFilter) {
        ArrayList<Filter<Log>> filters = new ArrayList<>(13);

        filters.add(getTimestampFilter(logFilter));
        filters.add(getSeverityFilter(logFilter));
        filters.add(getSubsystemFilter(logFilter));
        filters.add(getMachineFilter(logFilter));
        filters.add(getServerFilter(logFilter));
        filters.add(getThreadIdFilter(logFilter));
        filters.add(getUserIdFilter(logFilter));
        filters.add(getTransactionIdFilter(logFilter));
        filters.add(getDiagnosticContextIdFilter(logFilter));
        filters.add(getRawTimeFilter(logFilter));
        filters.add(getMessageIdFilter(logFilter));
        filters.add(getTextFilter(logFilter));

        filters.add(getRegexTextFilter(logFilter));

        filters.removeIf(Objects::isNull);
        return filters;
    }

    private static Filter<Log> getRegexTextFilter(LogFilter logFilter) {
        final String filter = logFilter.getRegexText();
        if (isEmptyFilter(filter))
            return null;
        return new RegexFilter<Log>(filter) {
            @Override
            public boolean accept(Log log) {
                return find(log.getText());
            }
        };
    }

    private static Filter<Log> getTextFilter(LogFilter logFilter) {
        final String filter = logFilter.getText();
        if (isEmptyFilter(filter))
            return null;
        return log -> log.getText().contains(filter);
    }

    private static Filter<Log> getMessageIdFilter(LogFilter logFilter) {
        final String filter = logFilter.getMessageId();
        if (isEmptyFilter(filter))
            return null;
        return log -> log.getMessageId().contains(filter);
    }

    private static Filter<Log> getRawTimeFilter(LogFilter logFilter) {
        final String filter = logFilter.getRawTime();
        if (isEmptyFilter(filter))
            return null;
        return log -> log.getRawTime().contains(filter);
    }

    private Filter<Log> getDiagnosticContextIdFilter(LogFilter logFilter) {
        final String filter = logFilter.getDiagnosticContextId();
        if (isEmptyFilter(filter))
            return null;
        return log -> log.getDiagnosticContextId().contains(filter);
    }

    private Filter<Log> getTransactionIdFilter(LogFilter logFilter) {
        final String filter = logFilter.getTransactionId();
        if (isEmptyFilter(filter))
            return null;
        return log -> log.getTransactionId().contains(filter);
    }

    private Filter<Log> getUserIdFilter(LogFilter logFilter) {
        final String filter = logFilter.getUserId();
        if (isEmptyFilter(filter))
            return null;
        return log -> log.getUserId().contains(filter);
    }

    private Filter<Log> getThreadIdFilter(LogFilter logFilter) {
        final String filter = logFilter.getThreadId();
        if (isEmptyFilter(filter))
            return null;
        return log -> log.getThreadId().contains(filter);
    }

    private Filter<Log> getMachineFilter(LogFilter logFilter) {
        final String filter = logFilter.getMachine();
        if (isEmptyFilter(filter))
            return null;
        return log -> log.getMachine().contains(filter);
    }

    private Filter<Log> getSubsystemFilter(LogFilter logFilter) {
        final String filter = logFilter.getSubsystem();
        if (isEmptyFilter(filter))
            return null;
        return log -> log.getSubsystem().contains(filter);
    }

    private Filter<Log> getSeverityFilter(LogFilter logFilter) {
        final Severity filter = logFilter.getSeverity();
        if (filter == null)
            return null;
        return log -> {
            if (log.getSeverity() != null)
                return log.getSeverity().ordinal() >= filter.ordinal();
            return false;
        };
    }

    private Filter<Log> getTimestampFilter(LogFilter logFilter) {
        final String filter = logFilter.getTimestamp();
        if (isEmptyFilter(filter))
            return null;
        return log -> log.getTimestamp().contains(filter);
    }

    private Filter<Log> getServerFilter(LogFilter logFilter) {
        final String filter = logFilter.getServer();
        if (isEmptyFilter(filter))
            return null;
        return log -> logFilter.getServer().contains(filter);
    }

    private static boolean isEmptyFilter(String filter) {
        return filter == null || filter.isEmpty();
    }
}
