package ru.klaw.restlog.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "LogFilter",
        description = "Фильтрует логи, каждый параметр проверяет содержиться ли подстрока в указанном сегменте лога. " +
                "Кроме параметра 'severity', который возвращает все логи с равным указанному параметру или выше.")
public class LogFilter extends Log {
    /**
     * Regex filter text
     */
    @ApiModelProperty("Принимает regex выражения, для поиска по тексту сообщения.")
    String regexText;

    public LogFilter(String timestamp, Severity severity,
                     String subsystem, String machine, String server,
                     String threadId, String userId, String transactionId, String diagnosticContextId,
                     String rawTime, String messageId, String text, String regexText) {
        super(timestamp, severity,
                subsystem, machine, server,
                threadId, userId, transactionId, diagnosticContextId,
                rawTime, messageId, text);
        this.regexText = regexText;
    }
}
