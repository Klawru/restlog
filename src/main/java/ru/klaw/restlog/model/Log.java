package ru.klaw.restlog.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Представляет собой строку из лога WebLogic")
public class Log {
    @ApiModelProperty("Time and date when the message originated, in a format that is specific to the locale.")
    String timestamp;

    @ApiModelProperty("Indicates the degree of impact or seriousness of the event reported by the message.")
    Severity severity;

    @ApiModelProperty("Indicates the subsystem of WebLogic Server that was the source of the message")
    String subsystem;

    @ApiModelProperty("Machine Name is the DNS name of the computer that hosts the server instance.")
    String machine;

    @ApiModelProperty("Server Name is the name of the WebLogic Server instance on which the message was generated.")
    String server;

    @ApiModelProperty("Thread ID is the ID that the JVM assigns to the thread in which the message originated.")
    String threadId;

    @ApiModelProperty("The user ID under which the associated event was executed.")
    String userId;

    @ApiModelProperty("Present only for messages logged within the context of a transaction.")
    String transactionId;

    @ApiModelProperty("Context information to correlate messages coming from a specific request or application.")
    String diagnosticContextId;

    @ApiModelProperty("The timestamp in milliseconds.")
    String rawTime;

    @ApiModelProperty("A unique six-digit identifier.")
    String messageId;

    @ApiModelProperty("A description of the event or condition.")
    String text;
}
